/* GStreamer object detection overlay
 * Copyright (C) <2022> Collabora Ltd.
 *  @author: Daniel Morin <daniel.morin@collabora.com>
 *
 * gstobjectdetectionoverlay.h
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_OBJECT_DETECTION_OVERLAY_H__
#define __GST_OBJECT_DETECTION_OVERLAY_H__

#include <gst/video/gstvideofilter.h>

G_BEGIN_DECLS

#define GST_TYPE_OBJECT_DETECTION_OVERLAY \
  (gst_object_detection_overlay_get_type())

#define GST_OBJECT_DETECTION_OVERLAY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_OBJECT_DETECTION_OVERLAY, \
      GstObjectDetectionOverlay))

#define GST_OBJECT_DETECTION_OVERLAY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_OBJECT_DETECTION_OVERLAY, \
      GstObjectDetectionOverlayClass))

#define GST_IS_OBJECT_DETECTION_OVERLAY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_OBJECT_DETECTION_OVERLAY))

#define GST_IS_OBJECT_DETECTION_OVERLAY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_OBJECT_DETECTION_OVERLAY))

GST_ELEMENT_REGISTER_DECLARE (objectdetectionoverlay);

typedef struct _GstObjectDetectionOverlay GstObjectDetectionOverlay;
typedef struct _GstObjectDetectionOverlayClass GstObjectDetectionOverlayClass;

struct _GstObjectDetectionOverlay
{
  GstVideoFilter parent;

  /*< private > */
  guint         od_outline_color;
  guint         od_outline_stroke_width;
  gboolean      draw_labels;
  guint         labels_color;
};

struct _GstObjectDetectionOverlayClass
{
  GstVideoFilterClass parent_class;
};

GType gst_object_detection_overlay_get_type (void);

G_END_DECLS
#endif /* __GST_OBJECT_DETECTION_OVERLAY_H__ */
