/*
 * GStreamer gstreamer-onnxoverlayrenderer
 *
 * Copyright (C) <2021> Collabora Ltd.
 *
 * gstonnxoverlayrenderer.c
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/**
 * SECTION:element-onnxoverlayrenderer
 * @title: onnxoverlayrenderer
 *

 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstonnxoverlayrenderer.h"

G_DEFINE_TYPE (GstOnnxOverlayRenderer, gst_onnx_overlay_renderer,
    GST_TYPE_BASE_OVERLAY_RENDERER);

GST_ELEMENT_REGISTER_DEFINE (onnx_overlay_renderer, "onnxoverlayrenderer",
    GST_RANK_PRIMARY, GST_TYPE_ONNX_OVERLAY_RENDERER);

static void
gst_onnx_overlay_renderer_class_init (GstOnnxOverlayRendererClass * klass)
{
  GstElementClass *element_class = (GstElementClass *) klass;

  gst_element_class_set_static_metadata (element_class,
      "OnnxOverlayRenderer", "Filter/Editor/Video",
      "Render text and vector graphics onto a video buffer",
      "Aaron Boxer <aaron.boxer@collabora.com>");
}

static void
gst_onnx_overlay_renderer_init (GstOnnxOverlayRenderer * onnx_overlay_renderer)
{
}
