/* GStreamer
 * Copyright (C) 2022 Collabora Ltd
 *
 * analyticmeta.c
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <gst/check/gstcheck.h>
#include <gst/analyticmeta/generic/gstanalysismeta.h>
#include <gst/analyticmeta/classification/gstanalysisclassificationmtd.h>
#include <gst/analyticmeta/object_detection/gstobjectdetectionmtd.h>
#include <gst/analyticmeta/tracking/gstobjecttrackingmtd.h>

GST_START_TEST (test_add_classification_meta)
{
  /* Verify we can create a relation metadata
   * and attach it classification mtd
   */
  GstBuffer *buf;
  GstAnalyticRelationMetaInitParams init_params = { 5, 150 };
  GstAnalyticRelationMeta *rmeta;
  GstAnalyticClsMtd *classificationMeta;
  gfloat conf_lvl[] = { 0.5f, 0.5f };
  const gchar *labels[] = { "dog", "cat" };

  buf = gst_buffer_new ();
  rmeta = gst_buffer_add_analytic_relation_meta (buf, &init_params);
  classificationMeta =
      gst_analytic_relation_add_analytic_cls_mtd (rmeta, conf_lvl, 2, labels,
      NULL, NULL);
  fail_unless (classificationMeta != NULL);
  gst_buffer_unref (buf);
}

GST_END_TEST;

GST_START_TEST (test_classification_meta_classes)
{
  /* Verify we can retrieve classification data
   * relation metadata
   */
  GstBuffer *buf;
  GstAnalyticRelationMetaInitParams init_params = { 5, 150 };
  GstAnalyticRelationMeta *rmeta;

  GstAnalyticClsMtd *classificationMeta, *classificationMeta2;
  buf = gst_buffer_new ();
  rmeta = gst_buffer_add_analytic_relation_meta (buf, &init_params);
  gfloat conf_lvl[] = { 0.6f, 0.4f };
  const gchar *labels[] = { "dog", "cat" };
  classificationMeta =
      gst_analytic_relation_add_analytic_cls_mtd (rmeta, conf_lvl, 2, labels,
      NULL, NULL);
  fail_unless (classificationMeta != NULL);
  fail_unless (gst_analytic_relation_get_length (rmeta) == 1);

  gint dogIndex =
      gst_analytic_cls_mtd_get_index_by_label (classificationMeta, "dog");
  fail_unless (dogIndex == 0);
  gfloat confLvl =
      gst_analytic_cls_mtd_get_level (classificationMeta, dogIndex);
  GST_LOG ("dog:%f", confLvl);
  assert_equals_float (confLvl, 0.6f);

  gint catIndex = gst_analytic_cls_mtd_get_index_by_quark (classificationMeta,
      g_quark_from_static_string ("cat"));
  confLvl = gst_analytic_cls_mtd_get_level (classificationMeta, catIndex);
  GST_LOG ("Cat:%f", confLvl);
  assert_equals_float (confLvl, 0.4f);
  assert_equals_int (gst_analytic_relatable_mtd_get_id (
          (GstAnalyticRelatableMtd *) classificationMeta), 0);

  conf_lvl[0] = 0.1f;
  conf_lvl[1] = 0.9f;
  classificationMeta2 =
      gst_analytic_relation_add_analytic_cls_mtd (rmeta, conf_lvl, 2, labels,
      NULL, NULL);
  fail_unless (classificationMeta2 != NULL);
  fail_unless (gst_analytic_relation_get_length (rmeta) == 2);
  assert_equals_int (gst_analytic_relatable_mtd_get_id (
          (GstAnalyticRelatableMtd *) classificationMeta2), 1);
  gst_buffer_unref (buf);
}

GST_END_TEST;

GST_START_TEST (test_add_relation_meta)
{
  /* Verify we can retrieve classification data
   * retrieved from relation metadata.
   * Relation metadata contain multiple
   * classification metadata
   */

  GstBuffer *buf;
  GstAnalyticClsMtd *classificationMeta[3];
  GstAnalyticRelationMetaInitParams init_params = { 5, 150 };
  GstAnalyticRelationMeta *relations;
  gint ids[3];

  buf = gst_buffer_new ();
  relations = gst_buffer_add_analytic_relation_meta (buf, &init_params);

  gfloat conf_lvl[] = { 0.6f, 0.4f };
  const gchar *pet_labels[] = { "dog", "cat" };

  classificationMeta[0] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      pet_labels, NULL, NULL);
  ids[0] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[0]);

  conf_lvl[0] = 0.6f;
  conf_lvl[1] = 0.4f;
  const gchar *kingdom_labels[] = { "plant", "animal" };

  classificationMeta[1] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      kingdom_labels, NULL, NULL);
  ids[1] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[1]);

  gint ret = gst_analytic_relation_meta_set_relation (relations,
      GST_ANALYTIC_REL_TYPE_IS_PART_OF,
      ids[0], (GstAnalyticRelatableMtd *) classificationMeta[0],
      ids[1], (GstAnalyticRelatableMtd *) classificationMeta[1]);

  fail_unless (ret == 0);
  gst_buffer_unref (buf);
}

GST_END_TEST;

GST_START_TEST (test_add_relation_inefficiency_reporting_cases)
{
  /*
   * Verify inefficiency of relation order is reported.
   */
  GstBuffer *buf;
  GstAnalyticClsMtd *classificationMeta[3];
  GstAnalyticRelationMetaInitParams init_params = { 2, 10 };
  GstAnalyticRelationMeta *relations;
  gint ids[3];
  gsize max_relation_order = 0, max_size = 0;

  buf = gst_buffer_new ();
  relations = gst_buffer_add_analytic_relation_meta (buf, &init_params);

  gfloat conf_lvl[] = { 0.6f, 0.4f };
  const gchar *pet_labels[] = { "dog", "cat" };

  classificationMeta[0] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      pet_labels, &max_relation_order, &max_size);
  fail_unless (max_relation_order == 0 && max_size != 0);
  ids[0] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[0]);

  conf_lvl[0] = 0.6f;
  conf_lvl[1] = 0.4f;
  const gchar *kingdom_labels[] = { "plant", "animal" };

  classificationMeta[1] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      kingdom_labels, &max_relation_order, &max_size);
  fail_unless (max_relation_order == 0 && max_size != 0);
  ids[1] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[1]);

  conf_lvl[0] = 0.6f;
  conf_lvl[1] = 0.4f;
  const gchar *gender_labels[] = { "male", "female" };

  classificationMeta[2] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      gender_labels, &max_relation_order, &max_size);
  fail_unless (max_relation_order != 0 && max_size != 0);
  ids[2] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[2]);

  gint ret = gst_analytic_relation_meta_set_relation (relations,
      GST_ANALYTIC_REL_TYPE_IS_PART_OF,
      ids[0], (GstAnalyticRelatableMtd *) classificationMeta[0],
      ids[1], (GstAnalyticRelatableMtd *) classificationMeta[1]);
  fail_unless (ret == 0);
  ret = gst_analytic_relation_meta_set_relation (relations,
      GST_ANALYTIC_REL_TYPE_IS_PART_OF,
      ids[0], (GstAnalyticRelatableMtd *) classificationMeta[0],
      ids[2], (GstAnalyticRelatableMtd *) classificationMeta[2]);

  fail_unless (ret == 0);
  gst_buffer_unref (buf);
}

GST_END_TEST;

GST_START_TEST (test_query_relation_meta_cases)
{
  GstBuffer *buf;
  GstAnalyticClsMtd *classificationMeta[3];
  GstAnalyticRelationMetaInitParams init_params = { 2, 150 };
  GstAnalyticRelationMeta *relations;
  gint ids[3];

  buf = gst_buffer_new ();
  relations = gst_buffer_add_analytic_relation_meta (buf, &init_params);

  gfloat conf_lvl[] = { 0.6f, 0.4f };
  const gchar *pet_labels[] = { "dog", "cat" };

  classificationMeta[0] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      pet_labels, NULL, NULL);
  ids[0] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[0]);

  conf_lvl[0] = 0.6f;
  conf_lvl[1] = 0.4f;
  const gchar *kingdom_labels[] = { "plant", "animal" };

  classificationMeta[1] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      kingdom_labels, NULL, NULL);
  ids[1] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[1]);

  conf_lvl[0] = 0.6f;
  conf_lvl[1] = 0.4f;
  const gchar *gender_labels[] = { "male", "female" };

  classificationMeta[2] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      gender_labels, NULL, NULL);
  ids[2] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[2]);

  // Pet is part of kingdom
  gst_analytic_relation_meta_set_relation (relations,
      GST_ANALYTIC_REL_TYPE_IS_PART_OF,
      ids[0], (GstAnalyticRelatableMtd *) classificationMeta[0],
      ids[1], (GstAnalyticRelatableMtd *) classificationMeta[1]);

  // Kingdom contain pet
  gst_analytic_relation_meta_set_relation (relations,
      GST_ANALYTIC_REL_TYPE_CONTAIN,
      ids[1], (GstAnalyticRelatableMtd *) classificationMeta[1],
      ids[0], (GstAnalyticRelatableMtd *) classificationMeta[0]);

  // Pet contain gender
  gst_analytic_relation_meta_set_relation (relations,
      GST_ANALYTIC_REL_TYPE_CONTAIN,
      ids[0], (GstAnalyticRelatableMtd *) classificationMeta[0],
      ids[2], (GstAnalyticRelatableMtd *) classificationMeta[2]);


  gboolean exist =
      gst_analytic_relation_meta_exist (relations, ids[0], ids[1], 1,
      GST_ANALYTIC_REL_TYPE_IS_PART_OF, NULL);
  fail_unless (exist == TRUE);

  exist =
      gst_analytic_relation_meta_exist (relations, ids[0], ids[2], 1,
      GST_ANALYTIC_REL_TYPE_IS_PART_OF, NULL);
  fail_unless (exist == FALSE);

  exist =
      gst_analytic_relation_meta_exist (relations, ids[0], ids[1], 1,
      GST_ANALYTIC_REL_TYPE_CONTAIN, NULL);
  fail_unless (exist == FALSE);

  GstAnalyticRelTypes cond =
      GST_ANALYTIC_REL_TYPE_IS_PART_OF | GST_ANALYTIC_REL_TYPE_CONTAIN |
      GST_ANALYTIC_REL_TYPE_RELATE_TO;
  exist =
      gst_analytic_relation_meta_exist (relations, ids[0], ids[2], 1, cond,
      NULL);
  fail_unless (exist == TRUE);

  cond = GST_ANALYTIC_REL_TYPE_CONTAIN | GST_ANALYTIC_REL_TYPE_RELATE_TO;
  exist =
      gst_analytic_relation_meta_exist (relations, ids[0], ids[1], 1, cond,
      NULL);
  fail_unless (exist == FALSE);

  exist =
      gst_analytic_relation_meta_exist (relations, ids[1], ids[2], 1,
      GST_ANALYTIC_REL_TYPE_CONTAIN, NULL);
  fail_unless (exist == FALSE);

  exist =
      gst_analytic_relation_meta_exist (relations, ids[1], ids[2], -1,
      GST_ANALYTIC_REL_TYPE_CONTAIN, NULL);
  fail_unless (exist == TRUE);

  exist =
      gst_analytic_relation_meta_exist (relations, ids[2], ids[1], -1,
      GST_ANALYTIC_REL_TYPE_CONTAIN, NULL);
  fail_unless (exist == FALSE);

  gst_buffer_unref (buf);
}

GST_END_TEST;

GST_START_TEST (test_path_relation_meta)
{
  GstBuffer *buf;
  GstAnalyticClsMtd *classificationMeta[3];
  GstAnalyticRelationMetaInitParams init_params = { 2, 150 };
  GstAnalyticRelationMeta *relations;
  gint ids[3];

  buf = gst_buffer_new ();
  relations = gst_buffer_add_analytic_relation_meta (buf, &init_params);

  gfloat conf_lvl[] = { 0.6f, 0.4f };
  const gchar *pet_labels[] = { "dog", "cat" };

  classificationMeta[0] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      pet_labels, NULL, NULL);
  ids[0] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[0]);

  conf_lvl[0] = 0.6f;
  conf_lvl[1] = 0.4f;
  const gchar *kingdom_labels[] = { "plant", "animal" };

  classificationMeta[1] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      kingdom_labels, NULL, NULL);
  ids[1] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[1]);

  conf_lvl[0] = 0.6f;
  conf_lvl[1] = 0.4f;
  const gchar *gender_labels[] = { "male", "female" };

  classificationMeta[2] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      gender_labels, NULL, NULL);
  ids[2] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[2]);

  // Pet is part of kingdom
  gst_analytic_relation_meta_set_relation (relations,
      GST_ANALYTIC_REL_TYPE_IS_PART_OF,
      ids[0], (GstAnalyticRelatableMtd *) classificationMeta[0],
      ids[1], (GstAnalyticRelatableMtd *) classificationMeta[1]);

  // Kingdom contain pet
  gst_analytic_relation_meta_set_relation (relations,
      GST_ANALYTIC_REL_TYPE_CONTAIN,
      ids[1], (GstAnalyticRelatableMtd *) classificationMeta[1],
      ids[0], (GstAnalyticRelatableMtd *) classificationMeta[0]);

  // Pet contain gender
  gst_analytic_relation_meta_set_relation (relations,
      GST_ANALYTIC_REL_TYPE_CONTAIN,
      ids[0], (GstAnalyticRelatableMtd *) classificationMeta[0],
      ids[2], (GstAnalyticRelatableMtd *) classificationMeta[2]);

  GSList *list = NULL;
  GstAnalyticRelTypes cond = GST_ANALYTIC_REL_TYPE_CONTAIN;
  gboolean exist =
      gst_analytic_relation_meta_exist (relations, ids[0], ids[2], -1, cond,
      &list);
  if (exist) {
    fail_unless (list != NULL);
    gint i = 0;
    gint path_ids[] = { 0, 2 };
    for (GSList * iter = list; iter; iter = iter->next, i++) {
      fail_unless (path_ids[i] == GPOINTER_TO_INT (iter->data));
    }

    fail_unless (i == 2);
  }

  cond = GST_ANALYTIC_REL_TYPE_CONTAIN;
  exist =
      gst_analytic_relation_meta_exist (relations, ids[1], ids[2], -1, cond,
      &list);
  if (exist) {
    gint i = 0;
    gint path_ids[] = { 1, 0, 2 };
    fail_unless (list != NULL);
    for (GSList * iter = list; iter; iter = iter->next, i++) {
      fail_unless (path_ids[i] == GPOINTER_TO_INT (iter->data));
    }
    fail_unless (i == 3);
  }
  gst_buffer_unref (buf);
}

GST_END_TEST;

GST_START_TEST (test_cyclic_relation_meta)
{
  GstBuffer *buf;
  GstAnalyticClsMtd *classificationMeta[3];
  GstAnalyticRelationMetaInitParams init_params = { 2, 150 };
  GstAnalyticRelationMeta *relations;
  gint ids[3];
  gfloat conf_lvl[2];
  const gchar *labels[] = { "attr1", "attr2" };

  buf = gst_buffer_new ();
  relations = gst_buffer_add_analytic_relation_meta_full (buf, &init_params);

  conf_lvl[0] = 0.5f;
  conf_lvl[1] = 0.5f;
  classificationMeta[0] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      labels, NULL, NULL);
  ids[0] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[0]);

  classificationMeta[1] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      labels, NULL, NULL);
  ids[1] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[1]);

  classificationMeta[2] =
      gst_analytic_relation_add_analytic_cls_mtd (relations, conf_lvl, 2,
      labels, NULL, NULL);
  ids[2] = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta[2]);

  // (0) -> (1)
  gst_analytic_relation_meta_set_relation (relations,
      GST_ANALYTIC_REL_TYPE_IS_PART_OF,
      ids[0], (GstAnalyticRelatableMtd *) classificationMeta[0],
      ids[1], (GstAnalyticRelatableMtd *) classificationMeta[1]);

  // (1)->(2)
  gst_analytic_relation_meta_set_relation (relations,
      GST_ANALYTIC_REL_TYPE_IS_PART_OF,
      ids[1], (GstAnalyticRelatableMtd *) classificationMeta[1],
      ids[2], (GstAnalyticRelatableMtd *) classificationMeta[2]);

  // (2) -> (0)
  gst_analytic_relation_meta_set_relation (relations,
      GST_ANALYTIC_REL_TYPE_IS_PART_OF,
      ids[2], (GstAnalyticRelatableMtd *) classificationMeta[2],
      ids[0], (GstAnalyticRelatableMtd *) classificationMeta[0]);

  GSList *list = NULL;
  GstAnalyticRelTypes cond = GST_ANALYTIC_REL_TYPE_CONTAIN;
  gboolean exist =
      gst_analytic_relation_meta_exist (relations, ids[0], ids[2], -1, cond,
      &list);
  fail_unless (exist == FALSE);

  cond = GST_ANALYTIC_REL_TYPE_IS_PART_OF;
  exist =
      gst_analytic_relation_meta_exist (relations, ids[0], ids[2], -1, cond,
      &list);
  fail_unless (exist == TRUE);
  if (exist) {
    gint i = 0;
    gint path_ids[] = { 0, 1, 2 };
    for (GSList * iter = list; iter; iter = iter->next, i++) {
      fail_unless (path_ids[i] == GPOINTER_TO_INT (iter->data));
    }
    fail_unless (i == 3);
  }

  gst_buffer_unref (buf);
}

GST_END_TEST;

GST_START_TEST (test_add_od_meta)
{
  GstBuffer *buf;
  GstAnalyticRelationMetaInitParams init_params = { 5, 150 };
  GstAnalyticRelationMeta *rmeta;
  GstAnalyticODMtd *od_mtd;
  buf = gst_buffer_new ();

  rmeta = gst_buffer_add_analytic_relation_meta (buf, &init_params);

  GQuark type = g_quark_from_string ("dog");
  guint x = 20;
  guint y = 20;
  guint w = 10;
  guint h = 15;
  gfloat loc_conf_lvl = 0.6f;
  od_mtd =
      gst_analytic_relation_add_analytic_od_mtd (rmeta, type, x, y, w, h,
      loc_conf_lvl, NULL, NULL);
  fail_unless (od_mtd != NULL);
  gst_buffer_unref (buf);
}

GST_END_TEST;

GST_START_TEST (test_od_meta_fields)
{
  GstBuffer *buf;
  GstAnalyticRelationMetaInitParams init_params = { 5, 150 };
  GstAnalyticRelationMeta *rmeta;
  GstAnalyticODMtd *od_mtd;
  buf = gst_buffer_new ();

  rmeta = gst_buffer_add_analytic_relation_meta (buf, &init_params);

  GQuark type = g_quark_from_string ("dog");
  guint x = 21;
  guint y = 20;
  guint w = 10;
  guint h = 15;
  gfloat loc_conf_lvl = 0.6f;
  od_mtd =
      gst_analytic_relation_add_analytic_od_mtd (rmeta, type, x, y, w, h,
      loc_conf_lvl, NULL, NULL);
  fail_unless (od_mtd != NULL);

  guint _x, _y, _w, _h;
  gfloat _loc_conf_lvl;
  gst_analytic_od_mtd_get_location (od_mtd, &_x, &_y, &_w, &_h, &_loc_conf_lvl);

  fail_unless (_x == x);
  fail_unless (_y == y);
  fail_unless (_w == w);
  fail_unless (_h == h);
  fail_unless (_loc_conf_lvl == loc_conf_lvl);

  gst_buffer_unref (buf);
}

GST_END_TEST;

GST_START_TEST (test_od_cls_relation)
{
  GstBuffer *buf;
  GstAnalyticClsMtd *classificationMeta;
  GstAnalyticRelationMetaInitParams init_params = { 5, 150 };
  GstAnalyticRelationMeta *rmeta;
  GstAnalyticODMtd *od_mtd;
  gint cls_id, od_id;

  buf = gst_buffer_new ();
  rmeta = gst_buffer_add_analytic_relation_meta (buf, &init_params);

  gfloat conf_lvl[] = { 0.7f, 0.3f };
  const gchar *pet_labels[] = { "dog", "cat" };

  classificationMeta =
      gst_analytic_relation_add_analytic_cls_mtd (rmeta, conf_lvl, 2,
      pet_labels, NULL, NULL);

  cls_id = gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta);

  GQuark type = g_quark_from_string ("dog");
  guint x = 21;
  guint y = 20;
  guint w = 10;
  guint h = 15;
  gfloat loc_conf_lvl = 0.6f;
  od_mtd =
      gst_analytic_relation_add_analytic_od_mtd (rmeta, type, x, y, w, h,
      loc_conf_lvl, NULL, NULL);
  od_id =
      gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *) od_mtd);
  fail_unless (od_mtd != NULL);

  gint ret = gst_analytic_relation_meta_set_relation (rmeta,
      GST_ANALYTIC_REL_TYPE_CONTAIN, od_id, (GstAnalyticRelatableMtd *) od_mtd,
      cls_id, (GstAnalyticRelatableMtd *) classificationMeta);
  fail_unless (ret == 0);

  ret = gst_analytic_relation_meta_set_relation (rmeta,
      GST_ANALYTIC_REL_TYPE_CONTAIN, cls_id,
      (GstAnalyticRelatableMtd *) classificationMeta, od_id,
      (GstAnalyticRelatableMtd *) od_mtd);
  fail_unless (ret == 0);

  gboolean exist = gst_analytic_relation_meta_exist (rmeta, od_id, cls_id, -1,
      GST_ANALYTIC_REL_TYPE_IS_PART_OF, NULL);
  fail_unless (exist == FALSE);

  GSList *list = NULL;
  exist = gst_analytic_relation_meta_exist (rmeta, od_id, cls_id, -1,
      GST_ANALYTIC_REL_TYPE_CONTAIN, &list);
  fail_unless (exist == TRUE);

  gint ids[2];
  gint i = 0;
  for (GSList * iter = list; iter; iter = iter->next, i++) {
    ids[i] = GPOINTER_TO_INT (iter->data);
    GST_LOG ("id=%u", ids[i]);
  }
  fail_unless (ids[0] == 1);
  fail_unless (ids[1] == 0);

  GstAnalyticRelatableMtd *mtd =
      gst_analytic_relation_meta_get_relatable_mtd (rmeta, ids[0]);

  GQuark mtd_type = gst_analytic_relatable_mtd_get_type (mtd);

  fail_unless (mtd_type == gst_analytic_od_mtd_get_type_quark ());

  guint _x, _y, _w, _h;
  gfloat _loc_conf_lvl;
  gst_analytic_od_mtd_get_location ((GstAnalyticODMtd *) mtd, &_x, &_y, &_w,
      &_h, &_loc_conf_lvl);
  fail_unless (_x == x);
  fail_unless (_y == y);
  fail_unless (_w == w);
  fail_unless (_h == h);
  fail_unless (_loc_conf_lvl == loc_conf_lvl);

  GST_LOG ("mtd_type:%s", g_quark_to_string (mtd_type));

  mtd = gst_analytic_relation_meta_get_relatable_mtd (rmeta, ids[1]);
  mtd_type = gst_analytic_relatable_mtd_get_type (mtd);

  fail_unless (mtd_type == gst_analytic_cls_mtd_get_type_quark ());
  gint index =
      gst_analytic_cls_mtd_get_index_by_label ((GstAnalyticClsMtd *) mtd,
      "dog");
  gfloat lvl =
      gst_analytic_cls_mtd_get_level ((GstAnalyticClsMtd *) mtd, index);
  GST_LOG ("dog %f [%d, %d %d, %d", lvl, _x, _y, _w, _h);

  fail_unless (lvl == 0.7f);
  index =
      gst_analytic_cls_mtd_get_index_by_label ((GstAnalyticClsMtd *) mtd,
      "cat");
  lvl = gst_analytic_cls_mtd_get_level ((GstAnalyticClsMtd *) mtd, index);
  fail_unless (lvl == 0.3f);

  GST_LOG ("mtd_type:%s", g_quark_to_string (mtd_type));
  GST_LOG ("cat %f [%d, %d %d, %d", lvl, _x, _y, _w, _h);

  gst_buffer_unref (buf);
}

GST_END_TEST;

GST_START_TEST (test_multi_od_cls_relation)
{
  GstBuffer *buf;
  GstAnalyticClsMtd *classificationMeta;
  GstAnalyticRelationMetaInitParams init_params = { 5, 300 };
  GstAnalyticRelationMeta *rmeta;
  GstAnalyticODMtd *od_mtd;
  gint cls_id, od_id, obj_ids[2], cls_ids[2], ids[2], i, ret;
  const gint dog_cls_index = 0;
  const gint cat_cls_index = 1;
  const gchar *pet_labels[] = { "dog", "cat" };
  gfloat cls_conf_lvl[2], lvl;
  GSList *list = NULL;
  gfloat _loc_conf_lvl;
  guint x, _x, y, _y, w, _w, h, _h;
  GQuark mtd_type, cls_type;
  GstAnalyticRelatableMtd *mtd;
  gpointer state = NULL;

  buf = gst_buffer_new ();
  rmeta = gst_buffer_add_analytic_relation_meta (buf, &init_params);

  /* Define first relation ObjectDetection -contain-> Classification */
  cls_conf_lvl[dog_cls_index] = 0.7f;
  cls_conf_lvl[cat_cls_index] = 0.3f;

  classificationMeta =
      gst_analytic_relation_add_analytic_cls_mtd (rmeta, cls_conf_lvl, 2,
      pet_labels, NULL, NULL);

  cls_ids[0] = cls_id =
      gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta);

  cls_type = g_quark_from_string ("dog");
  x = 21;
  y = 20;
  w = 10;
  h = 15;
  gfloat loc_conf_lvl = 0.6f;
  od_mtd =
      gst_analytic_relation_add_analytic_od_mtd (rmeta, cls_type, x, y, w, h,
      loc_conf_lvl, NULL, NULL);
  obj_ids[0] = od_id =
      gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *) od_mtd);
  fail_unless (od_mtd != NULL);

  ret = gst_analytic_relation_meta_set_relation (rmeta,
      GST_ANALYTIC_REL_TYPE_CONTAIN,
      od_id, (GstAnalyticRelatableMtd *) od_mtd,
      cls_id, (GstAnalyticRelatableMtd *) classificationMeta);
  fail_unless (ret == 0);
  GST_LOG ("Set rel Obj:%d -c-> Cls:%d", od_id, cls_id);

  /* Define second relation ObjectDetection -contain-> Classification */
  cls_conf_lvl[dog_cls_index] = 0.1f;
  cls_conf_lvl[cat_cls_index] = 0.9f;
  classificationMeta =
      gst_analytic_relation_add_analytic_cls_mtd (rmeta, cls_conf_lvl, 2,
      pet_labels, NULL, NULL);

  cls_ids[1] = cls_id =
      gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *)
      classificationMeta);
  cls_type = g_quark_from_string ("cat");
  x = 50;
  y = 21;
  w = 11;
  h = 16;
  loc_conf_lvl = 0.7f;
  od_mtd =
      gst_analytic_relation_add_analytic_od_mtd (rmeta, cls_type, x, y, w, h,
      loc_conf_lvl, NULL, NULL);
  fail_unless (od_mtd != NULL);
  obj_ids[1] = od_id =
      gst_analytic_relatable_mtd_get_id ((GstAnalyticRelatableMtd *) od_mtd);
  fail_unless (od_mtd != NULL);

  ret = gst_analytic_relation_meta_set_relation (rmeta,
      GST_ANALYTIC_REL_TYPE_CONTAIN,
      od_id, (GstAnalyticRelatableMtd *) od_mtd,
      cls_id, (GstAnalyticRelatableMtd *) classificationMeta);

  GST_LOG ("Set rel Obj:%d -c-> Cls:%d", od_id, cls_id);
  fail_unless (ret == 0);

  /* Query relations */

  /* Query relation between first object detection and first classification
   * and verify they are only related by CONTAIN relation OD relate to
   * CLASSIFICATION through a CONTAIN relation. */
  gboolean exist =
      gst_analytic_relation_meta_exist (rmeta, obj_ids[0], cls_ids[0], -1,
      GST_ANALYTIC_REL_TYPE_IS_PART_OF, NULL);
  fail_unless (exist == FALSE);

  exist = gst_analytic_relation_meta_exist (rmeta, obj_ids[0], cls_ids[1], -1,
      GST_ANALYTIC_REL_TYPE_CONTAIN, NULL);
  fail_unless (exist == FALSE);

  /* Query relation between second object detection and second classification
   * and verify they are only related by CONTAIN relation OD relate to
   * CLASSIFICATION through a CONTAIN relation.
   */
  exist = gst_analytic_relation_meta_exist (rmeta, obj_ids[1], cls_ids[1], -1,
      GST_ANALYTIC_REL_TYPE_CONTAIN, &list);
  fail_unless (exist == TRUE);

  /* Verify relation path between OD second (id=3) and Cls second (id=2)
   * is correct
   */
  i = 0;
  for (GSList * iter = list; iter; iter = iter->next, i++) {
    ids[i] = GPOINTER_TO_INT (iter->data);
    GST_LOG ("id=%u", ids[i]);
  }
  fail_unless (ids[0] == 3);
  fail_unless (ids[1] == 2);

  /* Verify the relatable metadata 3 is of correct type
   * (ObjectDetection). Verify it describe the correct
   * the correct data.
   */
  mtd = gst_analytic_relation_meta_get_relatable_mtd (rmeta, ids[0]);
  mtd_type = gst_analytic_relatable_mtd_get_type (mtd);
  fail_unless (mtd_type == gst_analytic_od_mtd_get_type_quark ());

  gst_analytic_od_mtd_get_location ((GstAnalyticODMtd *) mtd, &_x, &_y, &_w,
      &_h, &_loc_conf_lvl);
  fail_unless (_x == 50);
  fail_unless (_y == 21);
  fail_unless (_w == 11);
  fail_unless (_h == 16);
  fail_unless (_loc_conf_lvl == 0.7f);

  GST_LOG ("mtd_type:%s", g_quark_to_string (mtd_type));

  /* Verify the relatable metadata 2 is of correct type
   * (ObjectDetection).
   */
  mtd = gst_analytic_relation_meta_get_relatable_mtd (rmeta, ids[1]);
  mtd_type = gst_analytic_relatable_mtd_get_type (mtd);
  fail_unless (mtd_type == gst_analytic_cls_mtd_get_type_quark ());

  /* Verify data of the CLASSIFICATION retrieved */
  gint index =
      gst_analytic_cls_mtd_get_index_by_label ((GstAnalyticClsMtd *) mtd,
      "dog");
  lvl = gst_analytic_cls_mtd_get_level ((GstAnalyticClsMtd *) mtd, index);
  GST_LOG ("dog %f [%d, %d %d, %d", lvl, _x, _y, _w, _h);
  fail_unless (lvl == 0.1f);

  /* Verify data of the CLASSIFICATION retrieved */
  index =
      gst_analytic_cls_mtd_get_index_by_label ((GstAnalyticClsMtd *) mtd,
      "cat");
  lvl = gst_analytic_cls_mtd_get_level ((GstAnalyticClsMtd *) mtd, index);
  GST_LOG ("mtd_type:%s", g_quark_to_string (mtd_type));
  GST_LOG ("cat %f [%d, %d %d, %d", lvl, _x, _y, _w, _h);
  fail_unless (lvl == 0.9f);

  /* Retrieve relatable metadata related to the first object detection
   * through a CONTAIN relation of type CLASSIFICATION
   * Verify it's the first classification metadata
   */
  mtd = gst_analytic_relation_meta_get_direct_related (rmeta, obj_ids[0],
      GST_ANALYTIC_REL_TYPE_CONTAIN, gst_analytic_cls_mtd_get_type_quark (),
      &state);

  cls_id = gst_analytic_relatable_mtd_get_id (mtd);
  GST_LOG ("Obj:%d -> Cls:%d", obj_ids[0], cls_id);
  fail_unless (cls_id == cls_ids[0]);

  state = NULL;
  /* Retrieve relatable metadata related to the second object detection
   * through a CONTAIN relation of type CLASSIFICATION
   * Verify it's the first classification metadata
   */
  mtd = gst_analytic_relation_meta_get_direct_related (rmeta, obj_ids[1],
      GST_ANALYTIC_REL_TYPE_CONTAIN, gst_analytic_cls_mtd_get_type_quark (),
      &state);
  cls_id = gst_analytic_relatable_mtd_get_id (mtd);

  GST_LOG ("Obj:%d -> Cls:%d", obj_ids[1], cls_id);
  fail_unless (cls_id == cls_ids[1]);

  gst_buffer_unref (buf);
}

GST_END_TEST;

GST_START_TEST (test_add_track_meta)
{
  GstBuffer *buf1, *buf2;
  GstAnalyticRelationMetaInitParams init_params = { 5, 150 };
  GstAnalyticRelationMeta *rmeta;
  GstAnalyticTrackMtd *track_mtd;
  guint track_id;
  GstClockTime track_observation_time_1;

  /* Verify we can add multiple tracks to relation metadata
   */

  buf1 = gst_buffer_new ();
  rmeta = gst_buffer_add_analytic_relation_meta (buf1, &init_params);
  track_id = 1;
  track_observation_time_1 = GST_BUFFER_TIMESTAMP (buf1);
  track_mtd = gst_analytic_relation_add_analytic_track_mtd (rmeta, track_id,
      track_observation_time_1, NULL, NULL);
  fail_unless (track_mtd != NULL);

  gst_buffer_unref (buf1);

  buf2 = gst_buffer_new ();
  rmeta = gst_buffer_add_analytic_relation_meta (buf2, &init_params);
  track_id = 1;
  track_mtd = gst_analytic_relation_add_analytic_track_mtd (rmeta, track_id,
      track_observation_time_1, NULL, NULL);
  fail_unless (track_mtd);

  gst_buffer_unref (buf2);
}

GST_END_TEST;

static Suite *
analyticmeta_suite (void)
{

  Suite *s;
  TCase *tc_chain_cls;
  TCase *tc_chain_relation;
  TCase *tc_chain_od;
  TCase *tc_chain_od_cls;
  TCase *tc_chain_track;

  s = suite_create ("Analytic Meta Library");

  tc_chain_cls = tcase_create ("Classification Mtd");
  suite_add_tcase (s, tc_chain_cls);
  tcase_add_test (tc_chain_cls, test_add_classification_meta);
  tcase_add_test (tc_chain_cls, test_classification_meta_classes);

  tc_chain_relation = tcase_create ("Relation Meta");
  suite_add_tcase (s, tc_chain_relation);
  tcase_add_test (tc_chain_relation, test_add_relation_meta);
  tcase_add_test (tc_chain_relation,
      test_add_relation_inefficiency_reporting_cases);
  tcase_add_test (tc_chain_relation, test_query_relation_meta_cases);
  tcase_add_test (tc_chain_relation, test_path_relation_meta);
  tcase_add_test (tc_chain_relation, test_cyclic_relation_meta);

  tc_chain_od = tcase_create ("Object Detection Mtd");
  suite_add_tcase (s, tc_chain_od);
  tcase_add_test (tc_chain_od, test_add_od_meta);
  tcase_add_test (tc_chain_od, test_od_meta_fields);

  tc_chain_od_cls = tcase_create ("Object Detection <-> Classification Mtd");
  suite_add_tcase (s, tc_chain_od_cls);
  tcase_add_test (tc_chain_od_cls, test_od_cls_relation);
  tcase_add_test (tc_chain_od_cls, test_multi_od_cls_relation);

  tc_chain_track = tcase_create ("Tracking Mtd");
  suite_add_tcase (s, tc_chain_track);
  tcase_add_test (tc_chain_track, test_add_track_meta);
  return s;
}

GST_CHECK_MAIN (analyticmeta);
